/// <reference types="cypress" />

describe("Tickets", () => {
    beforeEach(() =>
        cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html")
    );

    //inserindo dados nos campos de input
    it("fills all text input fields", () => {
        const firstName = "Nelson";
        const lastName = "Neto";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("testandocypress@gmail.com");
        cy.get("#requests").type("Testando cypress");
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    //Selecionando uma opção no campo select
    it("select two tickets", () => {
        cy.get("#ticket-quantity").select("2");
    });

    //Selecionando radiobutton
    it("select 'vip' ticket type", () => {
        cy.get("#vip").check();
    });

    //interagindo co checkbox
    it("selects 'social media' checkbox", () => {
        cy.get("#social-media").check();
    });

    //interagindo com mais de uma checkbox
    it("selects 'friend' and 'publication', then uncheck 'friend' and 'publication'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    //Verificando se a tag h1 esta no header
    it("has 'TICKETBOX' header's heading", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    //Testando as verificações de e-mail inexistente e existente
    it("alerts on invalid email", () => {
        cy.get("#email")
            .as("email") //apelidando o elemento email e guardando o estado atual
            .type("testandoemail-gmail.com");

        cy.get("#email.invalid").should(("exist"));

        cy.get("@email")
            .clear()
            .type("testeemail@gmail.com");

        cy.get("#email.invalid").should("not.exist");
    });


    //Testando toda a aplicação - EndtoEnd
    it("fill and reset the form", () => {
        const firstName = "Nelson";
        const lastName = "Neto";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type("testandocypress@gmail.com");
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#friend").check();
        cy.get("#requests").type("Testando cypress");

        cy.get(".agreement p").should(
            "contain",
            `I, ${fullName}, wish to buy 2 VIP tickets.`
        );

        cy.get("#agree").click();

        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
            .as("submitButton")
            .should("not.be.disabled");

        cy.get("button[type='reset']").click();

        cy.get("@submitButton").should("be.disabled");

    });

    //Test custom
    it("fills mandatory fields using support command", () => {
        const customer = {
            firstName: "Nelson",
            lastName: "Neto",
            email: "nelson.neto@exemple.com"
        };

        cy.fillMandatoryField(customer);

        cy.get("button[type='submit']").as("submitButton").should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");
    })


});